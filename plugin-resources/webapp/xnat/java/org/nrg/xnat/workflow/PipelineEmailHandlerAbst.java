package org.nrg.xnat.workflow;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.model.WrkXnatexecutionenvironmentParameterI;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.WrkXnatexecutionenvironment;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.event.Event;
import org.nrg.xnat.notifications.NotifyProjectPipelineListeners;

import java.io.File;
import java.util.*;

/**
 * Created by flavin on 2/27/15.
 */
public abstract class PipelineEmailHandlerAbst extends WorkflowSaveHandlerAbst {
    static Logger logger = Logger.getLogger(PipelineEmailHandlerAbst.class);
    private static List<Integer> SENT_ALREADY= Lists.newArrayList();

    public final String DEFAULT_TEMPLATE_SUCCESS = "/screens/PipelineEmail_success.vm";
    public final String DEFAULT_SUBJECT_SUCCESS = "processed without errors";
    public final String DEFAULT_TEMPLATE_FAILURE = "/screens/PipelineEmail_failure.vm";
    public final String DEFAULT_SUBJECT_FAILURE = "";

    public void send(Event e, WrkWorkflowdata wrk, XnatExperimentdata expt,Map<String,Object> params,String emailTemplate, String emailSubject, String list_name, List<String> otherEmails) throws Exception{
        //temporary notification manager until we have the notification stuff flushed out.
        if (completed(e)) {
            (new NotifyProjectPipelineListeners(expt, emailTemplate, emailSubject, wrk.getUser(), params, list_name, otherEmails, "success")).send();
        } else {
            (new NotifyProjectPipelineListeners(expt,emailTemplate,emailSubject,wrk.getUser(),params,list_name,otherEmails, "failure")).send();
        }
    }

    public void standardPipelineEmailImpl(final Event e, WrkWorkflowdata wrk, final String pipelineName, final String template, final String subject, final String notificationFileName, Map<String,Object> params){

        try {
            String _pipelineName = wrk.getPipelineName();
            if (_pipelineName==null) {
                _pipelineName = (String)PoolDBUtils.ReturnStatisticQuery("select pipeline_name from wrk_workflowdata where wrk_workflowdata_id="+wrk.getWrkWorkflowdataId(),"pipeline_name",null,null);
            }
            if(StringUtils.endsWith(_pipelineName, pipelineName) && (completed(e) || failed(e)) && !SENT_ALREADY.contains(wrk.getWrkWorkflowdataId())) {
                if ( wrk.getPipelineName() == null) {
                    wrk = WrkWorkflowdata.getWrkWorkflowdatasByWrkWorkflowdataId(wrk.getWrkWorkflowdataId(),wrk.getUser(),false);
                }
                SchemaElement objXsiType;
                try {
                    objXsiType = SchemaElement.GetElement(wrk.getDataType());
                } catch (Throwable e1) {
                    logger.error("", e1);//this shouldn't happen
                    return;
                }

                //for now we are only worried about experiments
                if (objXsiType.getGenericXFTElement().instanceOf("xnat:experimentData") && wrk.getId() != null) {
                    final XnatExperimentdata expt = XnatExperimentdata.getXnatExperimentdatasById(wrk.getId(), wrk.getUser(), false);

                    params.put("justification", wrk.getJustification());

                    String _subject;
                    if (expt != null) {

                        if (wrk.getComments()!=null) {
                            String comments = wrk.getComments();

                            if (StringUtils.isNotBlank(comments)) {
                                ObjectMapper objectMapper = new ObjectMapper();
                                HashMap<String, String> commentsMap = null;
                                try {
                                    commentsMap = objectMapper.readValue(comments, new TypeReference<HashMap<String, String>>(){});
                                } catch (Exception e1) {
                                }

                                if (commentsMap == null) {
                                    params.put("comments",comments);
                                } else {
                                    params.putAll(commentsMap);
                                }
                            }
                        }

                        if(failed(e)) {
                            // Find pipeline logs
                            String builddir = null;
                            String label = null;
                            Map<String,File> attachments = Maps.newHashMap();
                            for (WrkXnatexecutionenvironmentParameterI pipelineParameter : ((WrkXnatexecutionenvironment)wrk.getExecutionenvironment()).getParameters_parameter()) {
                                if ("builddir".equals(pipelineParameter.getName())) {
                                    builddir = pipelineParameter.getParameter();
                                } else if ("label".equals(pipelineParameter.getName())) {
                                    label = pipelineParameter.getParameter();
                                }
                            }
                            if (StringUtils.isNotBlank(builddir) && StringUtils.isNotBlank(label)) {
                                String logPath = builddir + "/" + label + "/LOGS/";
                                File logDirFileObj = new File(logPath);
                                if (logDirFileObj.exists()) {
                                    File[] logFileObjs = logDirFileObj.listFiles();
                                    if (logFileObjs != null) {
                                        for (File logFileObj : logFileObjs ) {
                                            if (logFileObj.getName().endsWith(".log") || logFileObj.getName().endsWith(".err")) {
                                                attachments.put(logFileObj.getName(), logFileObj);
                                            }
                                        }
                                    }
                                }
                            }
                            params.put("attachments", attachments);
                            _subject = TurbineUtils.GetSystemName()+" update: Processing failed for " + expt.getLabel() +" "+subject;

                        } else {
                            _subject = TurbineUtils.GetSystemName()+" update: " + expt.getLabel() +" "+subject;
                        }
                        send(e, wrk, expt, params, template, _subject, notificationFileName, new ArrayList<String>());
                    }
                }

                SENT_ALREADY.add(wrk.getWrkWorkflowdataId());
            }
        } catch (Throwable e1) {
            logger.error("",e1);
        }
    }
}
