/**
 * functions for XNAT events (project)
 * xnat-templates/screens/xnat_projectData/xnat_projectData_summary_manage.vm
 */

XNAT.app.eventsManager = {};
XNAT.app.eventsManager.events = [];
XNAT.app.eventsManager.scripts = [];

$(function(){

    var eventsManager = XNAT.app.eventsManager;

    //var systemEvents = []; // ?
    //var systemScripts = []; // ?

    var $events_table = $('#events_table'),
        $no_event_handlers = $('#no_event_handlers');

    // return REST url with common parts pre-defined
    function restUrl( url, params, csrf ){
        // 'params' = array of query string params: ['format=json','sort=asc']
        params = params || [];
        if ( typeof csrf == 'undefined' || csrf.toString().toLowerCase() == 'true' ) {
            params.push('XNAT_CSRF=' + csrfToken);
        }
        url = (serverRoot || '') + '/' + url.replace(/^\//, '');
        url += (params.length) ? '?' + params.join('&') : '';
        return url;
    }

    eventsManager.initEventsMenu = function(){
        eventsManager.events = []; // reset array
        jQuery.ajax({
            type: 'GET',
            url: restUrl('/data/automation/events'),
            cache: false,
            success: function( response ){
                if ( (response.ResultSet.Result) && (response.ResultSet.Result.length) ) {
                    var options = "";
                    var length = response.ResultSet.Result.length;
                    for ( var index = 0; index < length; index++ ) {
                        var event = response.ResultSet.Result[index];
                        options += "<option value='" + event['event_id'] + "'>" + event['event_label'] + "</option>";
                        // add event to events array
                        eventsManager.events.push(event['event_id']);
                    }
                    $('#select_event').html(options);
                }
            },
            error: function( request, status, error ){
                xmodal.message('Error', 'An error occurred retrieving system events: [' + status + '] ' + error);
            }
        });
    }(/* execute now */);

    eventsManager.initScriptsMenu = function(){
        eventsManager.scripts = []; // reset array
        jQuery.ajax({
            type: 'GET',
            url: restUrl('/data/automation/scripts'),
            cache: false,
            success: function( response ){
                if ((response.ResultSet.Result) && (response.ResultSet.Result.length)) {
                    var options = "";
                    var length = response.ResultSet.Result.length;
                    for (var index = 0; index < length; index++) {
                        var scripts = response.ResultSet.Result[index];
                        options += "<option value='" + scripts['Script ID'] + "'>" + scripts['Script ID'] + ": " + scripts['Description'] + "</option>";
                        eventsManager.scripts.push(scripts['Script ID']);
                    }
                    $('#select_scriptId').html(options);
                }
            },
            error: function( request, status, error ){
                xmodal.message('Error', 'An error occurred retrieving system events: [' + status + '] ' + error);
            }
        });
    }(/* execute now */);

    eventsManager.initEventsTable = function(){

        // hide stuff
        $('#no_event_handlers, #events_table').hide();

        // Now get all the data and stick it in the table.
        jQuery.ajax({
            type: 'GET',
            url: restUrl('/data/projects/' + window.projectScope + '/automation/events'),
            cache: false,
            success: function( response ){
                var eventRows = "";
                if((response.ResultSet.Result) && (response.ResultSet.Result.length)) {
                    var length = response.ResultSet.Result.length;
                    for (var index = 0; index < length; index++) {
                        var eventHandler = response.ResultSet.Result[index];
                        eventRows += '<tr class="highlight">' +
                        '<td>' + eventHandler.event + '</td>' +
                        '<td>' + eventHandler.scriptId + '</td>' +
                        '<td>' + eventHandler.description + '</td>' +
                        '<td style="text-align: center;">' +
                        '<a href="javascript:void(0);" class="delete-handler" ' +
                        'data-handler="' + eventHandler.triggerId + '" title="Delete handler for event ' + eventHandler.event + '">delete</a>' +
                        '</td>' +
                        '</tr>';
                    }
                    $events_table.find('tbody').html(eventRows);
                    $events_table.show();
                } 
                else {
                    $no_event_handlers.show();
                }
            },
            error: function( request, status, error ){
                xmodal.message('Error', 'An error occurred retrieving event handlers for this project: [' + status + '] ' + error);
            },
            complete: function(){
                $('#accordion').accordion('refresh');
            }
        });
    }(/* execute now */);

    function addEventHandler(){
        xmodal.open({
            title: 'Add event handler',
            template: $('#addEventHandler'),
            width: 500,
            height: 300,
            buttons: {
                save: {
                    label: 'Save',
                    isDefault: true,
                    close: false,
                    action: doAddEventHandler
                },
                close: {
                    label: 'Cancel'
                }
            }
        });
    }

    function doAddEventHandler( xmodalObj ){

        var data = {
            event: xmodalObj.__modal.find('select.event').val(),
            scriptId: xmodalObj.__modal.find('select.scriptId').val(),
            description: xmodalObj.__modal.find('input').val()
        };

        // TODO: Should we let them name the trigger? Is that worthwhile? (yes)
        // var url = serverRoot + "/data/projects/" + window.projectScope + "/automation/events/" + triggerId + "?XNAT_CSRF=$!XNAT_CSRF";
        //var url = serverRoot + "/data/projects/" + window.projectScope + "/automation/events?XNAT_CSRF=$!XNAT_CSRF";

        jQuery.ajax({
            type: 'PUT',
            url: restUrl('/data/projects/' + window.projectScope + '/automation/events'),
            cache: false,
            data: data,
            dataType: "json",
            success: function(){
                xmodal.message('Success', 'Your event handler was successfully added.', 'OK', { 
                        action: function(){
                            // TODO: Change this to just delete the one row instead of repopulating the entire table.
                            // TODO: (cont'd) It's probably good to get fresh data
                            eventsManager.initEventsTable();
                            xmodal.closeAll(); // close 'parent' dialog                            
                        }  
                    }
                );
            },
            error: function( request, status, error ){
                xmodal.message('Error', 'An error occurred: [' + status + '] ' + error, 'Close', {
                    action: function(){
                        xmodal.closeAll()
                    }
                });
            }
        });
    }

    function deleteEventHandler( triggerId ){
        xmodal.confirm({
            title: 'Delete Event Handler?',
            content: 'Are you sure you want to delete the handler: <br><br><b>' + triggerId + '</b>?<br><br>Only the event handler will be deleted. The associated script will be available for future use.',
            width: 440,
            height: 240,
            okLabel: 'Delete',
            okClose: false, // don't close yet
            cancelLabel: 'Cancel',
            okAction: function(){
                doDeleteTrigger(triggerId);
            },
            cancelAction: function(){
                xmodal.message('Delete event handler cancelled', 'The event handler was not deleted.', 'Close');
            }
        });
    }

    function doDeleteTrigger( triggerId ){
        var url = restUrl('/data/automation/events/' + triggerId);
        if (window.jsdebug) console.log(url);
        jQuery.ajax({
            type: 'DELETE',
            url: url,
            cache: false,
            success: function(){
                xmodal.message('Success', 'The event handler was successfully deleted.', 'OK', {
                    action: function(){
                        // TODO: Change this to just delete the one row instead of repopulating the entire table.
                        eventsManager.initEventsTable();
                        xmodal.closeAll()
                    }
                });
            },
            error: function( request, status, error ){
                xmodal.message('Error', 'An error occurred: [' + status + '] ' + error, 'Close', {
                    action: function(){
                        xmodal.closeAll()
                    }
                });
            }
        });
    }


    // removed inline onclick attributes:
    $events_table.on('click', 'a.delete-handler', function(){
        deleteEventHandler($(this).data('handler'))
    });

    // *javascript* event handlers
    $('#add_script_button').on('click', addEventHandler);




});
